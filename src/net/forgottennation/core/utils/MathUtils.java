package net.forgottennation.core.utils;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class MathUtils {
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	public static final double sin(double angle){
		return Math.sin(angle);
	}
	
	public static final double tan(double angle){
		return Math.tan(angle);
	}
	public static final double cos(double angle){
		return Math.cos(angle);
	}
	
	public static Vector rotateFunction(Vector v , Location inc){
		double yawR = inc.getYaw() /180.0*Math.PI;
		double pitchR = inc.getPitch() /180.0*Math.PI;
		
		v = rotateAboutX(v, pitchR);
		v = rotateAboutY(v, -yawR);
		return v;
	}
	
	public static Vector rotateAboutX(Vector vect, double a){
		double Y = cos(a)*vect.getY() - sin(a)*vect.getZ();
		double Z = sin(a)*vect.getY() - sin(a)*vect.getZ();
		
		return vect.setY(Y).setZ(Z);
	}
	
	public static Vector rotateAboutY(Vector vect, double b){
		double X = cos(b)*vect.getX() + sin(b)*vect.getZ();
		double Z = -sin(b)*vect.getX() + cos(b)*vect.getZ();
		
		return vect.setX(X).setZ(Z);
	}
}
