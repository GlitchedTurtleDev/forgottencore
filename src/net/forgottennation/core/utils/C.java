package net.forgottennation.core.utils;

import org.bukkit.ChatColor;

public class C {
	public static String errorPrefix = ChatColor.RED + "Error >> " + ChatColor.RESET;
	public static String prefix = ChatColor.translateAlternateColorCodes('&', "&3&lDivine&f&lPvP &8&l// &e");
	public static String gamePrefix = ChatColor.translateAlternateColorCodes('&', "&3&lDivine&f&lPvP &8&l// &e");
	public static String noPerms = "Sorry, you require %perm% to execute that command.";
	public static String invalidArgs = "Please enter the correct arguments.";
}
