
package net.forgottennation.core.scoreboard;

import java.util.ArrayList;
import java.util.HashMap;

import net.forgottennation.core.Core;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.Rank;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class PlayerScoreboard {
    private FPlayer owner;
    private Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    private Objective sidebar;
    int clock = 0;

    public PlayerScoreboard(FPlayer owner) {
        if(owner.getPlayer() == null) return;
        this.owner = owner;
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        updateTag();
        sidebar = scoreboard.registerNewObjective("TheRift", "dummy");
        sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);
        sidebar.setDisplayName(ChatColor.GOLD + "TheRiftGames");
        owner.getPlayer().getPlayer().setScoreboard(scoreboard);
    }

    
    public FPlayer getOwner()
    {
    	return owner;
    }
    
    public void setTitle(String s)
    {
    	sidebar.setDisplayName(s);
    }
    
    public void updateTag() {
    	if(owner == null) return;
    	if(owner.getPlayer() == null) return;
    	for (Player p : Bukkit.getOnlinePlayers()) {
            FPlayer player = FPlayer.getPlayer(p);
            Team t = this.scoreboard.getTeam(p.getName());
            if (t == null) {
                t = this.scoreboard.registerNewTeam(p.getName());
            }
            if(!t.getPrefix().equals(player.getRank().getTag() + (player.getRank() == Rank.DEFAULT ? "" : " ")))
                t.setPrefix(player.getRank().getTag() + (player.getRank() == Rank.DEFAULT ? "" : " "));
            if(!t.hasEntry(p.getName()))
            	t.addEntry(p.getName());
        }
    }
    
	private HashMap<String, Integer> currentLines = new HashMap<>();
	private HashMap<Integer,String> lines = new HashMap<>();  
	
	public void setLine(int line, String value)
	{
		lines.put(line, value);
	}
	
	public void clearScoreboard()
	{
		lines.clear();
	}
	
	public void bakeScoreboard()
	{
    	if(owner == null) return;
    	if(owner.getPlayer() == null) return;
    	ArrayList<String> plxremove = new ArrayList<>();
		for(String line : currentLines.keySet())
		{
			int value = currentLines.get(line);
			boolean contains = false;
			checker: for(String s : lines.values())
			{
				if(s.equals(line)){
					contains = true;
					break checker;
				}
			}
			
			if(!contains)
			{
				plxremove.add(line);
			}
			
		}
		currentLines.clear();
		for(int i : lines.keySet())
		{
			String line = lines.get(i);
		
			sidebar.getScore(line).setScore(i);
			currentLines.put(line, i);
			
		}
		
		for(String s : plxremove)
		{
			scoreboard.resetScores(s);
		}
		clearScoreboard();
	}
}