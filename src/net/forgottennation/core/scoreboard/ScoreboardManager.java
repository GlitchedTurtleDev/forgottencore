package net.forgottennation.core.scoreboard;

import java.util.ArrayList;

import net.forgottennation.core.player.FPlayer;

import org.bukkit.event.Listener;

public class ScoreboardManager implements Listener{
	public ArrayList<PlayerScoreboard> scoreboards = new ArrayList<PlayerScoreboard>();
	
	public PlayerScoreboard createScoreboard(FPlayer p)
	{
		PlayerScoreboard scoreboard = new PlayerScoreboard(p);
		scoreboards.add(scoreboard);
		return scoreboard;
	}
	
	public void removeScoreboard(PlayerScoreboard score)
	{
		scoreboards.remove(score);
	}
	
	public PlayerScoreboard getScoreboard(FPlayer p){
		for(PlayerScoreboard ps : scoreboards){
			if(p == ps.getOwner()){
				return ps;
			}
		}
		return null;
	}
	
	public void updateTags()
	{
		for(PlayerScoreboard ps : scoreboards){
			ps.updateTag();
		}
	}
	
}
