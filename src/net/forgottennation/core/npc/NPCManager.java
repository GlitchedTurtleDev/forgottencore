package net.forgottennation.core.npc;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
public class NPCManager {
	public ArrayList<NPC> npcs = new ArrayList<NPC>();
	public void createNPC(String name, EntityType type, String command, Location loc)
	{
			npcs.add(new NPC(name, type, loc, command));
	}
}
