package net.forgottennation.core.npc;

import net.forgottennation.core.cooldown.Cooldown;
import net.forgottennation.core.listeners.FListener;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
public class NPC extends FListener {
	public Entity ent;
	public String name;
	public ArmorStand tag;
	public String command;
	public boolean commandEntity = true;
	public NPC(String tag, EntityType type, Location loc,String cmd)
	{
		command = cmd;
		name = tag;
		ent = loc.getWorld().spawnEntity(loc, type);
		
		this.tag = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
		
		//ent.setPassenger(this.tag);
		
		this.commandEntity = true;
		
		this.tag.setCustomName(tag);
		this.tag.setCustomNameVisible(true);
		//this.tag.setInvulnerable(true);
		this.tag.setVisible(false);
		noAI(this.ent);
		//((LivingEntity) this.ent).set
		this.register();
	}
	
	void noAI(Entity bukkitEntity) {
	    net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity) bukkitEntity).getHandle();
	    NBTTagCompound tag = nmsEntity.getNBTTag();
	    if (tag == null) {
	        tag = new NBTTagCompound();
	    }
	    nmsEntity.c(tag);
	    tag.setInt("NoAI", 1);
	    nmsEntity.f(tag);
	}
	
	public NPC(String tag, EntityType type, Location loc)
	{
		command = null;
		name = tag;
		ent = loc.getWorld().spawnEntity(loc, type);
		
		this.tag = (ArmorStand) loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
		
		//ent.setPassenger(this.tag);
		
		this.commandEntity = false;
		
		this.tag.setCustomName(tag);
		this.tag.setCustomNameVisible(true);
		//this.tag.setInvulnerable(true);
		this.tag.setVisible(false);
		this.tag.getLocation().setY(loc.getY() + 1.7);
		this.tag.setGravity(false);
		noAI(this.ent);
		//((LivingEntity) this.ent).set
		this.register();
	}
	
	
	@EventHandler
	public void onMove(PlayerMoveEvent event)
	{
		if(ent.getWorld() != event.getTo().getWorld()) return; 
		if(event.getTo().distance(ent.getLocation()) < 0.9)
		{
			event.setTo(event.getFrom());
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEntityEvent event)
	{
		if(event.getRightClicked() == ent)
		{
			//if(Cooldown.cool(event.getPlayer(), name, 500, true)) return;
			//Bukkit.broadcastMessage("you clicked me");
			if(commandEntity){
				//Bukkit.broadcastMessage("you clicked me chat");
				event.getPlayer().chat(command);
			} else {
				//Bukkit.broadcastMessage("you clicked me event");
				Bukkit.getPluginManager().callEvent(new NPCTriggerEvent(this, event.getPlayer()));
			}
		}
	}
}
