package net.forgottennation.core.cooldown;

import java.util.UUID;

import org.bukkit.entity.Player;

public class CooldownToken {
	private String name;
	private long expiry;
	private boolean silent;
	private UUID owner;
	
	public CooldownToken(Player owner, long expiry, String name, boolean silent)
	{
		this.owner = owner.getUniqueId();
		this.expiry = expiry;
		this.name = name;
		this.silent = silent;
	}

	public String getName() {
		return name;
	}

	public long getExpiry() {
		return expiry;
	}

	public UUID getOwner() {
		return owner;
	}

	public boolean isSilent() {
		return silent;
	}
	
}
