package net.forgottennation.core.cooldown;

import java.util.ArrayList;

import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.update.UpdateEvent;
import net.forgottennation.core.utils.C;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class Cooldown extends FListener{
	private static Cooldown instance = new Cooldown();
	public static ArrayList<CooldownToken> tokens = new ArrayList<>();
	
	private Cooldown()
	{
		this.register();
	}
	
	
	public static boolean cool(Player p, String name, long ms, boolean silent)
	{
		for(CooldownToken t : tokens)
		{
			if(t.getName().equals(name))
			{
				if(t.getOwner() == p.getUniqueId())
				{
					if(silent == false){
					p.sendMessage(C.errorPrefix + "You cannot use " + name + " for another " + (t.getExpiry()-System.currentTimeMillis())/1000 + " seconds");
					}
					return false;
				}
			}
		}
		tokens.add(new CooldownToken(p, System.currentTimeMillis() + ms, name,silent));
		if(!silent){
		p.sendMessage(C.prefix + "You used " + name);
		}
		return true;
	}
	
	public static boolean cool(Player p, String name, long ms)
	{
		for(CooldownToken t : tokens)
		{
			if(t.getName().equals(name))
			{
				if(t.getOwner() == p.getUniqueId())
				{
					p.sendMessage(C.errorPrefix + "You cannot use " + name + " for another " + (t.getExpiry()-System.currentTimeMillis())/1000 + " seconds");
					return false;
				}
			}
		}
		tokens.add(new CooldownToken(p, System.currentTimeMillis() + ms, name, false));
		p.sendMessage(C.prefix + "You used " + name);
		return true;
	}

	@EventHandler
	public void updateEvent(UpdateEvent e)
	{
		ArrayList<CooldownToken> delete = new ArrayList<>();
		for(CooldownToken t : tokens)
		{
			if(t.getExpiry() <= System.currentTimeMillis() + 1000)
			{
				if(!t.isSilent()){
				Bukkit.getPlayer(t.getOwner()).sendMessage(C.prefix + "You can now use " + t.getName());
				}
				delete.add(t);
			}
		}
		for(CooldownToken t : delete)
		{
			tokens.remove(t);
		}
	}

}
