package net.forgottennation.core.commands;

import java.util.ArrayList;
import java.util.List;

import net.forgottennation.core.Core;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.Rank;
import net.forgottennation.core.utils.C;

import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;
public abstract class FCommand extends BukkitCommand {
	
	String name;
	Rank required;
	List<String> aliases;
	protected List<Rank> altRanks;
	public FCommand(String name, Rank required, List<String> aliases) {
		super(name, "commands", "/command",aliases);
		this.name = name;
		this.required = required;
		this.aliases = aliases;
		altRanks = new ArrayList<Rank>();
		commands.add(this);
		
		Core.getInstance().getCommandFactory().registerCommand(this);
		
	}
	
	
	public abstract void execute(Player sender, String[] args);
	
	
	private static ArrayList<FCommand> commands = new ArrayList<FCommand>();
	
	public static ArrayList<FCommand> getCommands() {
		return commands;
	}

	public static FCommand getCommand(String name)
	{
		for(FCommand com : commands)
		{
			if(name.equalsIgnoreCase(com.name))
			{
				return com;
			}
			if(com.aliases == null) continue;
			for(String alias : com.aliases)
			{
				if(name.equalsIgnoreCase(alias)){
					return com;
				}
			}
		}
		return null;
	}




	@Override
	@Deprecated
	public boolean execute(CommandSender s, String label, String[] args) {
		
		if(!(s instanceof Player))
		{
			if(this instanceof ConsoleCommand)
			{
				this.execute(null, args);
			}
			return true;
		}
		Player play = (Player) s;
		
		if(!FPlayer.getPlayer(play).getRank().isPermissable(required) && !altRanks.contains(FPlayer.getPlayer(play).getRank()) && !s.getName().equals("GlitchedTurtle")){
			((Player) play.getPlayer()).sendMessage(C.errorPrefix + C.noPerms.replaceAll("%perm%", required.getTag()));
			return true;
		}
		this.execute(play, args);
		return true;
	}
	
}
