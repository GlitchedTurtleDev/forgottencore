package net.forgottennation.core.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.utils.C;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class FCommandListener extends FListener{
	public FCommandListener()
	{
		this.register();
	}
	
	public Location blockArea = new Location(Bukkit.getWorld("Survival1"),-583,57,315);
	
	@EventHandler
	public void onCommandPreProcess(PlayerCommandPreprocessEvent event)
	{
		if(event.getPlayer().getWorld().getName().equalsIgnoreCase("Survival1")){
		if(blockArea.distance(event.getPlayer().getLocation()) < 10){
			if(event.getMessage().equalsIgnoreCase("/towns"))
			{
				event.setCancelled(true);
				return;
			}
			if(event.getMessage().equalsIgnoreCase("/tpa"))
			{
				event.setCancelled(true);
				return;
			}
			if(event.getMessage().equalsIgnoreCase("/spawn"))
			{
				event.setCancelled(true);
				return;
			}
			
		}
		}
		if(FCommand.getCommand(event.getMessage().substring(1).split(" ")[0]) == null) return;
		event.setCancelled(true);
		FCommand command = FCommand.getCommand(event.getMessage().substring(1).split(" ")[0]);
		
		
		FPlayer play = FPlayer.getPlayer( event.getPlayer());
		
		List<String> abstractArgs = Arrays.asList(event.getMessage().split(Pattern.quote(" ")));
		
		List<String> arguments = new ArrayList<String>();
		
		for(String string : abstractArgs)
		{
			arguments.add(string);
		}
		
		
		arguments.remove(0);
		
		String[] args = new String[arguments.size()];
		
		int i = 0;
		for(String arg : arguments)
		{
			args[i] = arg;
			i++;
		}
		
		
		
		command.execute((Player) play.getPlayer(), args);
		
	}
}
