package net.forgottennation.core;

import me.tigerhix.lib.bossbar.BossbarLib;
import net.forgottennation.core.achievements.Achievement;
import net.forgottennation.core.achievements.AchievementManager;
import net.forgottennation.core.achievements.CountAchievement;
import net.forgottennation.core.chat.ChatControl;
import net.forgottennation.core.chat.ChatHandler;
import net.forgottennation.core.commands.CommandFactory;
import net.forgottennation.core.commands.management.CommandPlaypen;
import net.forgottennation.core.cosmetics.CosmeticsHub;
import net.forgottennation.core.db.DatabasePool;
import net.forgottennation.core.listeners.JoinLeaveHandler;
import net.forgottennation.core.listeners.UpdateHandler;
import net.forgottennation.core.scoreboard.ScoreboardManager;
import net.forgottennation.core.update.UpdateEvent;
import net.forgottennation.core.update.UpdateRate;
import net.forgottennation.core.utils.BungeeUtil;
import net.forgottennation.core.utils.FreezeManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Core extends JavaPlugin{
	private static Core instance;
	private ScoreboardManager scoreboardManager;
	private CommandFactory commandFactory = new CommandFactory();
	private DatabasePool databasePool;
	public AchievementManager achievementManager;
	public static Core getInstance()
	{
		return instance;
	}
	
	public AchievementManager getAchievementManager() {
		return achievementManager;
	}
	
	public CommandFactory getCommandFactory()
	{
		return commandFactory;
	}
	
	public ScoreboardManager getScoreboardManager() {
		return scoreboardManager;
	}

	public void onEnable(){
		instance = this;
		databasePool = new DatabasePool();
		BungeeUtil.triggerStart();
		BossbarLib.setPluginInstance(this);
		
		scoreboardManager = new ScoreboardManager();
		achievementManager = new AchievementManager();
		//bm = new BoosterManager();
		
		new ChatControl();
		new ChatHandler();
		new JoinLeaveHandler();
		new UpdateHandler();
		
		startUpdateEvent();
		
		new Achievement("First Steps", "Join the server","Global", 1000);
 		new Achievement("Parkour Legend", "Complete the Parkour","Global", 3000);
		new CountAchievement("Money Maker", "Earn 10,000 coins",5000, "Global", 10000);
		FreezeManager.setup();
		CosmeticsHub.setup();
		commandFactory.registerCoreCommands();
	}
	
	public void startUpdateEvent()
	{
		new BukkitRunnable()
		{
			int timer = 0;
			@Override
			public void run() 
			{
				timer ++;
				if(timer  % 1200 == 0) 
				{
					Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateRate.MINUTE));
					timer = 0;
				} else if(timer  % 20 == 0) 
				{
					Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateRate.SECOND));
				} else 
				{
					Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateRate.TICK));
				}
				
			}
			
		}.runTaskTimer(this, 1,1);
	}
	
	public DatabasePool getConnectionPool()
	{
		return databasePool;
	}
	
}
