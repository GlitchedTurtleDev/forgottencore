package net.forgottennation.core.player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import net.forgottennation.core.Core;
import net.forgottennation.core.achievements.CountAchievement;
import net.forgottennation.core.db.Database;
import net.forgottennation.core.db.DatabasePool;
import net.forgottennation.core.player.punishment.PunishCategory;
import net.forgottennation.core.player.punishment.PunishStatus;
import net.forgottennation.core.player.punishment.PunishType;
import net.forgottennation.core.player.punishment.Punishment;
import net.forgottennation.core.scoreboard.PlayerScoreboard;
import net.forgottennation.core.utils.C;
import net.forgottennation.core.utils.StringUtil;
import net.forgottennation.core.utils.UtilPrefix;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class FPlayer {
	private UUID player;
	private Rank rank = Rank.DEFAULT;
	private ArrayList<Punishment> punishments = new ArrayList<>();
	private HashMap<String, Long> stats = new HashMap<String,Long>();
	private HashMap<String, HashMap<String,Long>> gameStats = new HashMap<String,HashMap<String,Long>>();
	private PlayerScoreboard scoreboard;
	private double balance = 0;
	private long xp = 0;
	public Rank getRank(){
		return rank;
	}
	
	public Player getPlayer()
	{
		return Bukkit.getPlayer(player);
	}
	
	public long getXP()
	{
		return xp;	
	}
	
	public void addXP(long xp)
	{
		this.xp+=xp;
		updateBalances();
	}

	public void setXP(long xp)
	{
		this.xp=xp;
		updateBalances();
	}
	
	public Long getStat(String s)
	{
		return stats.get(s);
	}
	
	public double getBalance()
	{
		return balance;
	}
	
	public void addBalance(double coins)
	{
		balance+= coins;
		updateBalances();
		((CountAchievement) Core.getInstance().getAchievementManager().getAchievement("Money Maker")).complete(getPlayer(),(int)coins);
	}
	
	public void deductBalance(double coins)
	{
		balance-=coins;
		updateBalances();
	}
	
	public void setBalance(double coins)
	{
		balance = coins;
		updateBalances();
	}

	public Punishment getBanPunishment()
	{
		for(Punishment p : punishments){
			if(p.expiryTime > System.currentTimeMillis() || p.expiryTime < 0){
				if(p.status == PunishStatus.ACTIVE){
					if(p.type == PunishType.BAN){
						return p;
					}
				}
			}
		}
		return null;
	}
	
	public void updateBalances()
	{
		try  {
		String sql = "UPDATE `users` SET `bal`=?,`xp`=? WHERE `uuid`=?";
		Database d = DatabasePool.getDatabase();
		PreparedStatement p = d.prepare(sql);
		p.setDouble(1,getBalance());
		p.setLong(2, getXP());
		p.execute();
		d.close();
		} catch(Exception e){};
	}
	
	public PlayerScoreboard getScoreboard() {
		final FPlayer player = this;
		if(scoreboard == null)
		{
			Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), new Runnable()
			{

				@Override
				public void run() {
					// TODO Auto-generated method stub
					Core.getInstance().getScoreboardManager().createScoreboard(player);
				}
					
			}, 5l);
		}
		return scoreboard;
	}

	public void setScoreboard(PlayerScoreboard scoreboard) {
		this.scoreboard = scoreboard;
	}

	public Punishment getMutePunishment()
	{
		for(Punishment p : punishments){
			if(p.expiryTime > System.currentTimeMillis() || p.expiryTime < 0){
				if(p.status == PunishStatus.ACTIVE){
					if(p.type == PunishType.MUTE || p.type == PunishType.SMUTE){
						return p;
					}
				}
			}
		}
		return null;
	}
	
	public void setRank(Rank rank) {
		this.rank = rank;
	}
	
	public FPlayer(UUID play) {
		Player p = Bukkit.getPlayer(play);
		this.player = play;
		if(p != null) getScoreboard();
		try {
		Database d = DatabasePool.getDatabase();
		if(p != null){
			PreparedStatement state = d.prepare("SELECT * FROM `users` WHERE `uuid`=?");
		state.setString(1, p.getUniqueId().toString());
		if(!state.executeQuery().next()){
			PreparedStatement creation = d.prepare("INSERT INTO `users` (`uuid`) VALUES (?)");
			creation.setString(1, p.getUniqueId().toString());
			creation.execute();
			creation.close();
			d.close();
			return;
		}
		}
		String sql = "SELECT * FROM `users` WHERE `uuid`=?";
		PreparedStatement s = d.prepare(sql);
		s.setString(1, play.toString());
		ResultSet set = s.executeQuery();
		set.next();
		this.balance = set.getDouble("bal");
		this.rank = Rank.valueOf(set.getString("rank"));
		this.xp = set.getLong("xp");
		this.punishments = new ArrayList<>();
		d.close();
		} catch(Exception e) { e.printStackTrace(); };
		//Bukkit.broadcastMessage(ChatColor.DARK_PURPLE + "Join " + ChatColor.GRAY + ">> Welcome " + rank.getTag() + " "+play.getPlayer().getDisplayName());
	}
	
	public static ArrayList<FPlayer> players = new ArrayList<FPlayer>();
	
	public static FPlayer getPlayer(Player play)
	{
		FPlayer p = null;
		for(FPlayer pl : players)
		{
			if(pl.getPlayer() == play){
				p = pl;
				break;
			}
		}
		if(p == null){
			return createNewUser(play);
		} else {
			return p;
		}
	}
	
	public static FPlayer getPlayer(UUID u)
	{
		for(FPlayer pl : players)
		{
			if(pl.player == u){
				return pl;
			}
		}
		
		return new FPlayer(u);
	}
	
	public void applyPunishment(PunishType punishtype,PunishCategory category, long length ,FPlayer punisher,String reason, int severity){
		this.applyPunishment(punishtype, category, length, punisher.getPlayer().getUniqueId().toString(), reason, severity);
	}
	
	public void applyPunishment(PunishType punishtype,PunishCategory category, long length , String punisher,String reason,int severity){
		Punishment punish = new Punishment(punishtype, PunishStatus.ACTIVE,category, punisher, player.toString(), System.currentTimeMillis(), System.currentTimeMillis() + length, reason);
		punishments.add(punish);
		FPlayer play = this;
		switch(punishtype)
		{
			case BAN:
				if(getPlayer() != null){
					if(punish.expiryTime < 0)
					{
						getPlayer().kickPlayer(ChatColor.GOLD + "" + ChatColor.BOLD + "\nTheRiftGames\n\n" + ChatColor.RESET + ChatColor.WHITE + "Your account has been banned for " + ChatColor.GOLD + play.getBanPunishment().reason + ChatColor.RESET + "\nYour ban lasts " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Indefinitely");
					} else {
						
						long timeLeft = play.getBanPunishment().expiryTime - System.currentTimeMillis();
						String left = StringUtil.getFormattedTimeString(timeLeft/1000);
						getPlayer().kickPlayer(ChatColor.GOLD + "" + ChatColor.BOLD + "\nTheRiftGames\n\n" + ChatColor.RESET + ChatColor.WHITE + "Your account has been banned for " + ChatColor.GOLD + play.getBanPunishment().reason + ChatColor.RESET + "\nYour ban lasts " + ChatColor.GOLD + "for another " + left);
					
					}
				}
				break;
			case MUTE:
				if(getPlayer() != null) getPlayer().sendMessage(ChatColor.BLUE + "Punishment >>" + ChatColor.RESET + "You have been muted for " + reason);
				break;
			case SMUTE:
				break;
			case WARN:
				if(getPlayer() != null){
					getPlayer().sendMessage(UtilPrefix.getPrefix("Warn") + "You have been given a friendly warning for " + reason + "\nLater punishments for a similar reason may be longer.");
				}
				break;
			default:
				break;
				
			
		}
		//Core.getInstance().getSQLDatabase().applyPunishment(punish);
		if(Bukkit.getPlayer(punisher) != null) Bukkit.getPlayer(punisher).sendMessage(C.prefix + "Processed punishment");
		Bukkit.broadcastMessage(ChatColor.BLUE + "Punishment >> " + ChatColor.RESET + (getPlayer() != null ? getPlayer().getDisplayName() : "A player") + " has received a " + punishtype.name() + " for " + reason + " (sev-" + severity + "-" + category.toString() +")");
	}
	
	
	public void removePunishment(PunishStatus status, Punishment punish)
	{
		punishments.remove(punish);
		//Core.getInstance().getSQLDatabase().setPunishmentStatus(punish.id, status);
	}
	
	public static FPlayer createNewUser(Player play){
		FPlayer fplay = new FPlayer(play.getUniqueId());
		players.add(fplay);
		return fplay;
	}
}
