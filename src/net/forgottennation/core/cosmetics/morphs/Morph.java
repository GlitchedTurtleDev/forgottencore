package net.forgottennation.core.cosmetics.morphs;

import java.util.ArrayList;
import java.util.List;

import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.update.UpdateEvent;
import net.forgottennation.core.update.UpdateRate;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class Morph extends FListener{
	FDisguise disguise;
	Player owner;
	public Morph(Player sender, EntityDisguise type)
	{
		disguise = new FDisguise(sender, type);
		this.register();
		
		disguise.setType(type);
		
		owner = sender;
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			try {
				if(p == owner) continue;
				disguise.sendDisguise(p);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public Player getOwner()
	{
		return owner;
	}
	
	public FDisguise getDisguise()
	{
		return disguise;
	}
	
	
	@Override
	public void finalize()
	{
		unregister();
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event)
	{
		try {
			disguise.sendDisguise(event.getPlayer());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	  
	List<String> seen = new ArrayList<String>();
	
	@EventHandler
	public void onUpdate(UpdateEvent event)
	{
		if(event.getUpdateType() != UpdateRate.SECOND) return;
		if(event.getUpdateType() == UpdateRate.MINUTE) seen.clear();
		for(Player p : Bukkit.getOnlinePlayers())
		{
			try {
				if(p == owner) continue;
				if(seen.contains(p.getName())) continue;
				seen.add(p.getName());
				disguise.updateDisguise(p);
				disguise.setHelmet(owner.getInventory().getHelmet());
				disguise.setChestplate(owner.getInventory().getChestplate());
				disguise.setLeggings(owner.getInventory().getLeggings());
				disguise.setBoots(owner.getInventory().getBoots());
				disguise.setItemInHand(owner.getInventory().getItemInHand());
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@EventHandler
	public void teleport(PlayerTeleportEvent event)
	{
		if(event.getPlayer() == owner) seen.clear();
		seen.remove(event.getPlayer().getName());
	}
	
	@Override
	public void unregister()
	{
		try {
			disguise.removeDisguise();
		} catch (ReflectiveOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.unregister();
	}
	
}
