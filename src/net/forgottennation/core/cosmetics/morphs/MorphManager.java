package net.forgottennation.core.cosmetics.morphs;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

public class MorphManager {
	public MorphManager()
	{
		
	}
	
	public HashMap<UUID,Morph> activeMorphs = new HashMap<>();
	
	
	public Morph getActiveMorph(Player p)
	{
		return activeMorphs.get(p.getUniqueId());
	}
	
	public void setMorph(Player p, Morph morph)
	{
		activeMorphs.put(p.getUniqueId(), morph);
	}
	
	public void removeMorph(Player p){
		getActiveMorph(p).unregister();
		activeMorphs.remove(p.getUniqueId());
	}
}
