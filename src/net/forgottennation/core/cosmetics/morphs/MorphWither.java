package net.forgottennation.core.cosmetics.morphs;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkull;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class MorphWither extends Morph{
	//BossBar bossbar;
	public MorphWither(Player sender)
	{
		super(sender,EntityDisguise.WITHER_BOSS);
		
		
		for(int i = 0; i < 36 ; i++)
		{
			sender.getInventory().setItem(i, new ItemStack(Material.FIREBALL));
		}
		sender.setAllowFlight(true);
		sender.setFlying(true);
		sender.setMaxHealth(300);
		sender.setHealth(300);
		/*bossbar = Bukkit.createBossBar(sender.getName(), BarColor.BLUE, BarStyle.SOLID,BarFlag.DARKEN_SKY);
		for(Player  : Bukkit.getOnlinePlayers()){
			bossbar.addPlayer(p);
		}*/
	}
	
	@EventHandler
	public void onRightClick(PlayerInteractEvent e)
	{
		if(getOwner() ==  e.getPlayer())
		{
			if(!(e.getItem().getType() == Material.FIREBALL)) return;
            Player p = (Player) e.getPlayer();
                e.setCancelled(true);
                p.launchProjectile(WitherSkull.class).setVelocity(e.getPlayer().getEyeLocation().getDirection().multiply(1.5));
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e)
	{
		if(getOwner() == e.getEntity())
		{
			//bossbar.setProgress((((Damageable) e.getEntity()).getHealth() - e.getDamage())/300);
		}
	}
	
	@EventHandler
	public void onDamage(EntityRegainHealthEvent e)
	{
		if(getOwner() == e.getEntity())
		{
			//bossbar.setProgress((((Damageable) e.getEntity()).getHealth() + e.getAmount())/300);
		}
	}
	
	@Override
	public void unregister()
	{
		//bossbar.removeAll();
		owner.setAllowFlight(false);
		owner.setFlying(false);
		owner.setMaxHealth(20);
		super.unregister();
	}
	
}
