package net.forgottennation.core.cosmetics.particles;

import net.forgottennation.core.Core;
import net.forgottennation.core.utils.MathUtils;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class FireSerpent {
	public static void fireSerpent(final Player player){
		Runnable run = new Runnable(){
			double t = 20;
			@Override
			public void run() {
				t = t + Math.PI/200;
				
				final Location loc = player.getLocation();
				
				double x; double y; double z;
				double x1; double y1; double z1;
				double x2; double y2; double z2;
				double r = 0.5;
				x2 = MathUtils.sin(2*t);
				y2 = MathUtils.cos(t)*2;
				z2 = MathUtils.sin(3*t);
				
				t-= Math.PI/200;
				
				x1 = MathUtils.sin(2*t);
				y1 = 2*MathUtils.cos(t);
				z1 = MathUtils.sin(3*t);
				t+= Math.PI/200;
				Vector dir = new Vector(x2-x1,y2-y1,z2-z1);
				
				Location loc2 = new Location(player.getWorld(),0,0,0).setDirection(dir.normalize());
				
				loc2.setDirection(dir.normalize());
				
				if(t%1<0.1){
					player.getWorld().playSound(loc, Sound.CREEPER_HISS, 1, 1);
				}
				
				for(double i = 0; i <= 2*Math.PI; i = i+Math.PI/12){
					x = 0.2*t;
					y = r*MathUtils.sin(i)+2*MathUtils.sin(10*t)+2.8;
					z = r*MathUtils.cos(i);
					
					Vector v = new Vector(x,y,z);
					v = MathUtils.rotateFunction(v, loc2);
					loc.add(v.getX(),v.getY(), v.getZ());
					player.getWorld().spigot().playEffect(loc, Effect.FLAME, 2,2,0,0,0,0,10,100);
					if(i==0){
						player.getWorld().spigot().playEffect(loc, Effect.LAVA_POP, 2,2,0,0,0,0,10,100);
					}
					loc.subtract(v.getX(),v.getY(),v.getZ());
				}
			}
			
		};
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Core.getInstance(), run, 0, 0);
	}
}
