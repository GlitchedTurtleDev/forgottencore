package net.forgottennation.core.update;

public enum UpdateRate {
	TICK,
	SECOND,
	MINUTE;
}
