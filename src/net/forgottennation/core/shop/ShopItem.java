package net.forgottennation.core.shop;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import net.forgottennation.core.db.Database;
import net.forgottennation.core.db.DatabasePool;
import net.forgottennation.core.listeners.FListener;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.punishment.PunishmentGUI;
import net.forgottennation.core.utils.C;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;


public class ShopItem extends FListener {
	String name;
	Player owner;
	Inventory inv;
	int meta = -1;
	int cost;
	public ShopItem(String name, Player owner, int cost)
	{
		this.name = name;
		this.owner = owner;
		this.cost = cost;
	}
	
	public boolean isOwned()
	{
		
		String sql = "SELECT * FROM `shop` WHERE `owner`=? AND `item`=?";
		
		 try {
			Database d = DatabasePool.getDatabase();
			
			PreparedStatement state = d.prepare(sql);
		
			state.setString(1, owner.getUniqueId().toString());
			state.setString(2, name);
			
			ResultSet set = state.executeQuery();
			
			while(set.next())
			{
				return true;
			}
			d.close();
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return false;
		
	}
	
	public void purchaseItem()
	{
		if(isOwned()){
			owner.sendMessage(C.prefix + "You already own this item!");
			return;
		}
		
		inv = Bukkit.createInventory(null, 54, ChatColor.GREEN + "Purchase Item");
		
		for(int i = 19 ; i < 40 ; i++ )
		{
			inv.setItem(i, 
					PunishmentGUI.getItem(ChatColor.GREEN + "" + ChatColor.BOLD + "Purchase Item", 
							Material.EMERALD_BLOCK, Arrays.asList(ChatColor.GRAY + "Confirm that you would ", ChatColor.GRAY + "like to purchase this item")));
			
			inv.setItem(i+4, 
					PunishmentGUI.getItem(ChatColor.RED + "" + ChatColor.BOLD + "Cancel Purchase", 
							Material.REDSTONE_BLOCK, Arrays.asList(ChatColor.GRAY + "Cancel the requested ", ChatColor.GRAY + "purchase")));
			if(i % 3 == 0) i += 6;
		}
		
		owner.openInventory(inv);
		
		this.register();
	}
	
	public void confirmPurchase(int meta,boolean bypassLimit)
	{
		FPlayer fp = FPlayer.getPlayer(owner);
		if(isOwned()) return;
		if(fp.getBalance() - cost <= 0 && !bypassLimit)
		{
			owner.sendMessage(C.errorPrefix + "Insufficient Funds");
			return;
		}
		
		if(!bypassLimit) fp.deductBalance(cost);
		
		String sql = "INSERT INTO `forgottennation`.`shop` (`item`, `owner`,`meta`) VALUES (?, ?, ?)";
		
		try {
			PreparedStatement statement = DatabasePool.getDatabase().prepare(sql);
			
			statement.setString(1, name);
			statement.setString(2,owner.getUniqueId().toString());
			statement.setInt(3,meta);
			statement.execute();
		} catch(Exception e){}

		owner.sendMessage(C.prefix + "You have purchased " + name);
		owner.closeInventory();
	}
	
	public void confirmPurchase(){ confirmPurchase(meta,false); }
	
	@EventHandler
	public void onInvClicK(InventoryClickEvent event)
	{
		if(event.getWhoClicked() != owner) return;
		
		if(event.getCurrentItem().getType() == Material.EMERALD_BLOCK)
		{
			this.confirmPurchase();
		}
		if(event.getCurrentItem().getType() == Material.REDSTONE_BLOCK)
		{
			event.getWhoClicked().closeInventory();
		}
		
		event.setCancelled(true);
		event.setResult(Result.DENY);
	}
	
	@EventHandler
	public void onInvClose(InventoryCloseEvent event)
	{
		if(event.getPlayer() != owner) return;
		
		this.unregister();
	}
	
}
