package net.forgottennation.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import net.forgottennation.core.Core;

public class FListener implements Listener{
	public void register()
	{
		Bukkit.getPluginManager().registerEvents(this, Core.getInstance());
	}
	public void unregister()
	{
		HandlerList.unregisterAll(this);
	}
}
