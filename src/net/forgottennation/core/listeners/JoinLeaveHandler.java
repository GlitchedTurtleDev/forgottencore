package net.forgottennation.core.listeners;

import java.io.ObjectInputStream.GetField;
import java.text.SimpleDateFormat;

import me.tigerhix.lib.bossbar.BossbarLib;
import net.forgottennation.core.Core;
import net.forgottennation.core.cosmetics.treasure.TreasureChest;
import net.forgottennation.core.cosmetics.treasure.TreasureLocations;
import net.forgottennation.core.player.FPlayer;
import net.forgottennation.core.player.Rank;
import net.forgottennation.core.utils.StringUtil;
import net.forgottennation.core.utils.UtilTab;
import net.forgottennation.core.utils.UtilTitle;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinLeaveHandler extends FListener{
	public JoinLeaveHandler()
	{
		this.register();
	}
	
	@EventHandler
	public void onConnect(PlayerLoginEvent event)
	{
		//event.disallow(Result.KICK_OTHER, ChatColor.GOLD + "" + ChatColor.BOLD + "\nTheRiftGames\n\n" + ChatColor.RESET + ChatColor.WHITE + "Thank you for playing the rift. Sadly, the server has been shutdown and the server will soon be closed entirely. In the future, the rift may reopen. But this may not be for a long time. Enjoy your life. - GlitchedTurtle");
		//return;
		
		FPlayer play = FPlayer.getPlayer(event.getPlayer().getUniqueId());
		if(play.getBanPunishment() != null){
			
			if(play.getBanPunishment().expiryTime < 0)
			{
				event.disallow(Result.KICK_BANNED,ChatColor.GOLD + "" + ChatColor.BOLD + "\nTheRiftGames\n\n" + ChatColor.RESET + ChatColor.WHITE + "Your account has been banned for " + ChatColor.GOLD + play.getBanPunishment().reason + ChatColor.RESET + "\nYour ban lasts " + ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Indefinitely");
			} else {
				
				long timeLeft = play.getBanPunishment().expiryTime - System.currentTimeMillis();
				String left = StringUtil.getFormattedTimeString(timeLeft/1000);
				event.disallow(Result.KICK_BANNED,ChatColor.GOLD + "" + ChatColor.BOLD + "\nTheRiftGames\n\n" + ChatColor.RESET + ChatColor.WHITE + "Your account has been banned for " + ChatColor.GOLD + play.getBanPunishment().reason + ChatColor.RESET + "\nYour ban lasts " + ChatColor.GOLD + "for another " + left);
			
			}
			return;
		}
	}
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		FPlayer play = FPlayer.getPlayer(event.getPlayer());
		
		Core.getInstance().getScoreboardManager().updateTags();
		
		Core.getInstance().getAchievementManager().getAchievement("First Steps").complete(event.getPlayer());
		
		event.setJoinMessage(ChatColor.GREEN + "+ " + ChatColor.RESET + play.getRank().getTag() + event.getPlayer().getDisplayName());
		BossbarLib.getHandler().getBossbar(event.getPlayer()).setMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "THERIFT.TK                           THERIFT.TK");
	
		UtilTab.setTabList(event.getPlayer(), ChatColor.GOLD + "" + ChatColor.BOLD + "The Rift Games\n" + ChatColor.RESET + "A unique minigames network!", "therift.tk");
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event){
		FPlayer play = FPlayer.getPlayer(event.getPlayer());
		FPlayer.players.remove(play);
    	Core.getInstance().getScoreboardManager().scoreboards.remove(play.getScoreboard());
		event.setQuitMessage(ChatColor.RED + "- " + ChatColor.RESET + play.getRank().getTag() + event.getPlayer().getDisplayName());
	}
	
}
